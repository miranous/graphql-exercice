const { makeExecutableSchema } = require('graphql-tools');
const resolvers = require('./resolvers.js');

typeDefs = [`
    type Item {
        id: Int
        name: String
        price: String
    }
    type Query {
        GetItems: [Item]
        GetBasket: [Item]
    }
    input ItemInput {
        id: Int!
        name: String
        price: String
    }
    type Mutation {
        AddItem(id: Int!): [Item]
        DeleteItem(id: Int!): [Item]
    }
`];

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

module.exports = schema;