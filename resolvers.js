const _ = require('lodash');

const Items = [
    { 
        id: 1, 
        name: "Iphone 6",
        price: "10 $" }
        ,
    { 
        id: 2, 
        name: "Dell laptop",
        price: "20 $" 
    },
    { 
        id: 3, 
        name: "MacBook Pro",
        price: "30 $" 
    }
];

const Basket = [];

const resolvers = {
    Query: {
        GetItems: () => {
            return Items;
        },
        GetBasket: () => {
            return Basket;
        },
    },
    Mutation: {
        AddItem: (root, { id }) => {
            let selected = _.find(Items, {id});
            if(!selected){
                return new Error("ID not found")
            }else{
                Basket.push(selected);
            }
            return Basket;
        },
        DeleteItem: (root, { id }) => {
            const index = Items.findIndex(x => x.id === id)
            if(index !== -1){
                Basket.splice(index, 1);
            }else{
                return new Error("ID not found")
            }
            
            return Basket;
        }
    }
};

module.exports = resolvers;