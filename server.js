const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require("./schema.js");
const cors = require("cors");
const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors());

app.listen(PORT);

console.log("GraphQL API Server up and running at localhost:" + PORT);

app.use(
	"/graphql",
	graphqlHTTP({
		schema: schema,
		graphiql: true
	})
);
